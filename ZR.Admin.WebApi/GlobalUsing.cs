﻿global using ZR.Common;
global using Microsoft.AspNetCore.Authorization;
global using Infrastructure;
global using Infrastructure.Attribute;
global using Infrastructure.Enums;
global using Infrastructure.Model;
global using Mapster;